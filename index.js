const express = require('express');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');

const PORT = process.env.PORT || 4000;

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Spectra Tenant API',
      version: '2.0.0',
      description: 'Spectra - Plug n Play monitoring and management solution for IOT to help opertion and maintenance be more productive',
    },
    servers: [
      {
        url: `http://localhost:${PORT}`,
      }
    ]
  },
  apis: ['./routes/tenant.js', './routes/user_group.js'],
};

const specs = swaggerJsDoc(options);

const app = express();

app.use('/', swaggerUI.serve, swaggerUI.setup(specs));

app.listen(PORT, () => console.log(`The server is running on port ${PORT}`));


