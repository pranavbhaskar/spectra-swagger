/**
 * @swagger
 * components:
 *  schemas:
 *    UserGroup:
 *      type: object
 *      required: 
 *        - name
 *        - users
 *      properties:
 *        _id: 
 *          type: string
 *          description: The auto generated UserGroup id
 *        name: 
 *          type: string
 *          description: The UserGroup name
 *        users:
 *          type: array
 *          items:
 *            type: string
 *          description: The User Ids of the UserGroup
 *        tenant:
 *          type: string
 *          description: The Tenant id of UserGroup
 *      example:
 *        _id: 'ObjectId(5abqdkslci34zkb)'
 *        name: Group A
 *        users: ['ObjectId(6abqdidgbl09akc)', 'ObjectId(8qckli44bmsdacc)', 'ObjectId(2f4x0iaacnxwf0c)']
 *        tenant: 'ObjectId(8qbclz9lcx5czmf)'
 *    NewUserGroup:
 *      type: object
 *      required: 
 *        - name
 *        - users
 *      properties:
 *        name: 
 *          type: string
 *          description: The UserGroup name
 *        users:
 *          type: array
 *          items:
 *            type: string
 *          description: The User Ids of the UserGroup
 *        tenant:
 *          type: string
 *          description: The Tenant id of UserGroup
 *      example:
 *        name: Group A
 *        users: ['ObjectId(6abqdidgbl09akc)', 'ObjectId(8qckli44bmsdacc)', 'ObjectId(2f4x0iaacnxwf0c)']
 *        tenant: 'ObjectId(8qbclz9lcx5czmf)'
 */        

/**
 * @swagger
 * tags:
 *  name: UserGroups
 *  description: The UserGroups of Spectra.
 */

/**
 * @swagger
 *  /groups: 
 *    get: 
 *      summary: Get a list of all UserGroups 
 *      tags: [UserGroups]
 *      responses: 
 *        200: 
 *          description: The list of all UserGroups  
 *          content: 
 *            application/json: 
 *              schema: 
 *                type: array 
 *                items: 
 *                  $ref: '#/components/schemas/UserGroup'
 *        500:
 *          description: Internal server error
 *
 */ 

/**
 * @swagger
 *  /groups/{page}: 
 *    get: 
 *      summary: Get a paginated list of UserGroups 
 *      tags: [UserGroups]
 *      parameters:
 *        - in: path
 *          name: page
 *          schema:
 *            type: integer
 *          required: true
 *          description: The page no. for paginated list of UserGroups
 *      responses: 
 *        200: 
 *          description: The list of paginated UserGroups for page no. 
 *          content: 
 *            application/json: 
 *              schema: 
 *                type: array 
 *                items: 
 *                  $ref: '#/components/schemas/UserGroup'
 *        400:
 *          description: Page no. out of limit
 */ 

/**
 * @swagger
 *  /groups/search: 
 *    get: 
 *      summary: Get a list of UserGroups that matches the search query
 *      tags: [UserGroups]
 *      parameters:
 *        - in: query
 *          name: field
 *          schema:
 *            type: string
 *          required: true
 *          description: Search UserGroups for field
 *        - in: query
 *          name: value
 *          schema:
 *            type: string
 *          required: true
 *          description: Search UserGroups for value
 *      responses: 
 *        200: 
 *          description: The list of UserGroups that matches the search query  
 *          content: 
 *            application/json: 
 *              schema: 
 *                type: array 
 *                items: 
 *                  $ref: '#/components/schemas/UserGroup'
 *        404:
 *          description: No UserGroup description matching the query found
 *        
 */ 

/**
 * @swagger
 * /groups/{id}:
 *   get:
 *     summary: Get the UserGroup by id
 *     tags: [UserGroups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The UserGroup id
 *     responses:
 *       200:
 *         description: The UserGroup with id.
 *         content:
 *           application/json:
 *             schema: 
 *               $ref: '#/components/schemas/UserGroup'
 *       404:
 *         description: The UserGroup was not found
 */

/**
 * @swagger
 * /groups:
 *   post:
 *     summary: Create a new UserGroup
 *     tags: [UserGroups]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewUserGroup'
 *     responses:
 *       200:
 *         description: The tenant was succesfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/UserGroup'
 *       400:
 *         description: Bad Request, invalid syntax
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /groups/{id}:
 *   put:
 *     summary: Update the UserGroup by the id
 *     tags: [UserGroups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: String
 *         required: true
 *         description: The UserGroup id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UserGroup'
 *     responses:
 *       200:
 *         description: The UserGroup was updated
 *         content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/UserGroup'
 *       404:
 *         description: The UserGroup was not found
 *       400:
 *         description: Bad Request, invalid syntax
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /groups/{id}:
 *   delete:
 *     summary: Remove UserGroup by the id
 *     tags: [UserGroups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: String
 *         required: True
 *         description: The UserGroup id
 *     responses:
 *       200:
 *         description: The UserGroup was deleted
 *       404:
 *         description: The UserGroup was not found
 */ 
