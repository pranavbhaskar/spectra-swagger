/**
 * @swagger
 * components:
 *  schemas:
 *    Tenant:
 *      type: object
 *      required: 
 *        - name
 *        - user_groups
 *      properties:
 *        _id: 
 *          type: string
 *          description: The auto generated Tenant id
 *        name: 
 *          type: string
 *          description: The Tenant name
 *        user_groups:
 *          type: array
 *          items:
 *            type: string
 *          description: The User Group Ids of the Tenant
 *      example:
 *        _id: 'ObjectId(5abqdkslci34zkb)'
 *        name: Tenant A1
 *        user_groups: ['ObjectId(6abqdidgbl09akc)', 'ObjectId(8qckli44bmsdacc)', 'ObjectId(2f4x0iaacnxwf0c)']
 *    NewTenant:
 *      type: object
 *      required: 
 *        - name
 *        - user_groups
 *      properties:
 *        name:
 *          type: string
 *          description: The Tenant name
 *        user_groups:
 *          type: array
 *          items: 
 *            type: string
 *          description: The User Group Ids of the Tenant
 *      example:
 *        name: Tenant A1
 *        user_groups: ['ObjectId(6abqdidgbl09akc)', 'ObjectId(8qckli44bmsdacc)', 'ObjectId(2f4x0iaacnxwf0c)']
 */        

/**
 * @swagger
 * tags:
 *  name: Tenants
 *  description: The Tenants of Spectra.
 */

/**
 * @swagger
 *  /tenants: 
 *    get: 
 *      summary: Get a list of all tenants 
 *      tags: [Tenants]
 *      responses: 
 *        200: 
 *          description: The list of all tenants  
 *          content: 
 *            application/json: 
 *              schema: 
 *                type: array 
 *                items: 
 *                  $ref: '#/components/schemas/Tenant'
 *        500:
 *          description: Internal server error
 *
 */ 

/**
 * @swagger
 *  /tenants/{page}: 
 *    get: 
 *      summary: Get a paginated list of tenants 
 *      tags: [Tenants]
 *      parameters:
 *        - in: path
 *          name: page
 *          schema:
 *            type: integer
 *          required: true
 *          description: The page no. for paginated list of tenants
 *      responses: 
 *        200: 
 *          description: The list of paginated tenants for page no. 
 *          content: 
 *            application/json: 
 *              schema: 
 *                type: array 
 *                items: 
 *                  $ref: '#/components/schemas/Tenant'
 *        400:
 *          description: Page no. out of limit
 */ 

/**
 * @swagger
 *  /tenants/search: 
 *    get: 
 *      summary: Get a list of tenants that matches the search query
 *      tags: [Tenants]
 *      parameters:
 *        - in: query
 *          name: field
 *          schema:
 *            type: string
 *          required: true
 *          description: Search tenants for field
 *        - in: query
 *          name: value
 *          schema:
 *            type: string
 *          required: true
 *          description: Search tenants for value
 *      responses: 
 *        200: 
 *          description: The list of tenants that matches the search query  
 *          content: 
 *            application/json: 
 *              schema: 
 *                type: array 
 *                items: 
 *                  $ref: '#/components/schemas/Tenant'
 *        404:
 *          description: No tenant description matching the query found
 *        
 */ 

/**
 * @swagger
 * /tenants/{id}:
 *   get:
 *     summary: Get the tenant by id
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The Tenant id
 *     responses:
 *       200:
 *         description: The Tenant with id.
 *         content:
 *           application/json:
 *             schema: 
 *               $ref: '#/components/schemas/Tenant'
 *       404:
 *         description: The tenant was not found
 */

/**
 * @swagger
 * /tenants:
 *   post:
 *     summary: Create a new tenant
 *     tags: [Tenants]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewTenant'
 *     responses:
 *       200:
 *         description: The tenant was succesfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Tenant'
 *       400:
 *         description: Bad Request, invalid syntax
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /tenants/{id}:
 *   put:
 *     summary: Update the tenant by the id
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: String
 *         required: true
 *         description: The tenant id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Tenant'
 *     responses:
 *       200:
 *         description: The tenant was updated
 *         content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/Tenant'
 *       404:
 *         description: The tenant was not found
 *       400:
 *         description: Bad Request, invalid syntax
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /tenants/{id}:
 *   delete:
 *     summary: Remove tenant by the id
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: String
 *         required: True
 *         description: The tenant id
 *     responses:
 *       200:
 *         description: The tenant was deleted
 *       404:
 *         description: The tenant was not found
 */ 
